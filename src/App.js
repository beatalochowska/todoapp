import React, { Component } from 'react';
import './App.css';

import { TasksView } from "views";

class App extends Component {
  render() {
    return <TasksView />;
  }
}

export default App;
