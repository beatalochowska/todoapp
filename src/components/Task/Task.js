import React, { Component } from "react";

class Task extends Component {


    renderName = () => {
        const { id, name, isDone, handleChangeName } = this.props;
        if (isDone) {
            return name.length === 0 ? "brak zadania" : name
        }

        return <input type="text" value={name} placeholder="Wpisz tutaj swoje zadanie" onChange={event => handleChangeName(id, event.target.value)} />
    }

    render() {
        const { id, number, isDone, handleDelete, toggleStatus} = this.props;
        return (
            <p style={{ textDecoration: isDone ? "line-through" : "" }}>
                Zad {number}. 
                {this.renderName()}
                <button onClick={() => toggleStatus(id)}>niezrobione</button>
                <button onClick={() => handleDelete(id)}>usuń zadanie</button>
            </p>
        );
        
    }



}
export default Task;