import React, { Component } from "react";
import { Task } from "components";

function createUUID() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

class Notes extends Component {

    state = {
        noteIds: [],
        notes: {}
    }

    render() {

        const createTask = (element, index) => {
            const { id, name, isDone } = this.state.notes[element]
            return <Task key={index} number={index + 1} id={id} name={name} isDone={isDone} handleChangeName={this.handleChangeName} handleDelete={this.handleDelete} toggleStatus={this.toggleStatus} />
        }

        return (
            <section>
                <h1>To do App:</h1>
                {this.state.noteIds.map(createTask)}
                <button onClick={this.handleAdd}>dodaj zadanie</button>
            </section>
        );
    }

    handleAdd = () => {
        const initTaskId = createUUID();
        const initTask = {
            name: "",
            id: initTaskId,
            isDone: false
        }
        this.setState({
            noteIds: [...this.state.noteIds, initTaskId],
            notes: { ...this.state.notes, [initTaskId]: initTask }
        })

    }

    handleChangeName = (id, taskName) => {
        this.setState({
            notes: {
                ...this.state.notes,
                [id]: {
                    ...this.state.notes[id],
                    name: taskName,
                }
            }
        })
    }

    handleDelete = id => {
        let toDeleteTab = this.state.noteIds
        toDeleteTab.splice(toDeleteTab.indexOf(id), 1)
        this.setState({
            noteIds: toDeleteTab
        })

    }


    toggleStatus = id => {
        const currentStatus = this.state.notes[id].isDone
        this.setState({
            notes: {
                ...this.state.notes,
                [id]: {
                    ...this.state.notes[id],
                    isDone: !currentStatus,
                }
            }
        })
    }

}

export default Notes;